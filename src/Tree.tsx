import { Component, useEffect, useMemo, useState } from 'react'
import './Tree.css'

type Node = {
  id: number
  name: string
  subIds: number[]
  mainId: number
}

type UINode = Node & {
  childNodes: UINode[]
  parentNode: UINode | null
}

class Tree extends Component {
  state = {
    data: [] as Node[],
  }
  checkedIds: Record<number, boolean> = {}
  rootNodes: UINode[] = []

  componentDidMount() {
    this.setData(loadData())
  }

  setData(data: Node[]) {
    this.setState({ data, checkedIds: {} })

    let nodes: Record<number, UINode> = {}

    data.forEach(node => {
      nodes[node.id] = { ...node, childNodes: [], parentNode: null }
    })

    function walkNode(node: UINode) {
      node.subIds.forEach(subId => {
        if (!node.childNodes.find(subNode => subNode.id == subId)) {
          let childNode = nodes[subId]
          node.childNodes.push(childNode)
          childNode.parentNode = node
          walkNode(childNode)
        }
      })
    }

    let rootNodes = Object.values(nodes).filter(node => node.id === node.mainId)
    rootNodes.forEach(walkNode)

    this.rootNodes = rootNodes
  }

  render() {
    return (
      <div>
        <h1>Tree Root</h1>
        {this.rootNodes.map(node => this.renderNode(node))}
      </div>
    )
  }

  setChecked(node: UINode, checked: boolean) {
    this.nestDown(node, checked)
    if (!checked) {
      this.nestUp(node, checked)
    }
    this.forceUpdate()
  }

  nestUp(node: UINode, checked: boolean) {
    this.checkedIds[node.id] = checked
    if (node.parentNode) {
      this.nestUp(node.parentNode, checked)
    }
  }

  nestDown(node: UINode, checked: boolean) {
    this.checkedIds[node.id] = checked
    node.childNodes.forEach(node => this.nestDown(node, checked))
  }

  renderNode(node: UINode) {
    return (
      <div className="Node" key={node.id}>
        <input
          type="checkbox"
          checked={this.checkedIds[node.id] || false}
          onChange={event => {
            let input = event.target
            this.setChecked(node, input.checked)
          }}
        />{' '}
        #{node.id} - {node.name}
        {node.childNodes.map(node => this.renderNode(node))}
      </div>
    )
  }
}

function loadData() {
  let data: Node[] = [
    { id: 1, name: 'Fruit', subIds: [2, 3], mainId: 1 },
    { id: 2, name: 'Apple', subIds: [], mainId: 1 },
    { id: 3, name: 'Banana', subIds: [], mainId: 1 },
    { id: 4, name: 'Applicants', subIds: [5, 6], mainId: 4 },
    { id: 5, name: 'Laptop', subIds: [], mainId: 4 },
    { id: 6, name: 'Smart Phone', subIds: [7, 8], mainId: 4 },
    { id: 7, name: 'Android Phone', subIds: [10, 11, 12], mainId: 4 },
    { id: 8, name: 'iPhone Phone', subIds: [], mainId: 4 },
    { id: 9, name: 'Others', subIds: [], mainId: 9 },
    { id: 10, name: 'Samsung Phone', subIds: [], mainId: 4 },
    { id: 11, name: 'Sony Phone', subIds: [], mainId: 4 },
    { id: 12, name: 'HTC Phone', subIds: [], mainId: 4 },
  ]
  return data
}

export default Tree
